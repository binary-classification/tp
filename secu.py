import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import pandas_profiling
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import precision_score, precision_recall_curve, average_precision_score
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from inspect import signature


df = pd.read_csv("antivirus_dataset.csv", sep='|')


# Panda profiling
# profile = df.profile_report(title='Pandas Profiling Report', plot={'histogram': {'bins': 8}})
# profile.to_file(output_file="output.html")

label = df["legitimate"]

to_drop = [
    "md5",
    "VersionInformationSize",
    "SizeOfUninitializedData",
    "SizeOfStackReserve",
    "SizeOfStackCommit",
    "SizeOfOptionalHeader",
    "SizeOfInitializedData",
    "SizeOfImage",
    "SizeOfHeapReserve",
    "SizeOfHeapCommit",
    "SizeOfHeaders",
    "SizeOfCode",
    "SectionsMinVirtualsize",
    "SectionsMinRawsize",
    "SectionsMinEntropy",
    "SectionsMeanVirtualsize",
    "SectionsMeanRawsize",
    "SectionMaxVirtualsize",
    "SectionMaxRawsize",
    "SectionAlignment",
    "ResourcesNb",
    "ResourcesMinSize",
    "ResourcesMinEntropy",
    "ResourcesMeanSize",
    "ResourcesMeanEntropy",
    "ResourcesMaxSize",
    "ResourcesMaxEntropy",
    "NumberOfRvaAndSizes",
    "Name",
    "MinorSubsystemVersion",
    "MinorOperatingSystemVersion",
    "MinorLinkerVersion",
    "MinorImageVersion",
    "MajorOperatingSystemVersion",
    "MajorLinkerVersion",
    "MajorImageVersion",
    "LoaderFlags",
    "LoadConfigurationSize",
    "ImportsNbOrdinal",
    "ImportsNbDLL",
    "ImportsNb",
    "ImageBase",
    "FileAlignment",
    "ExportNb",
    "DllCharacteristics",
    "CheckSum",
    "BaseOfData",
    "BaseOfCode",
    "AddressOfEntryPoint",
    "legitimate"
]

new_df = df.drop(columns=to_drop)

train, test, train_y, test_y = train_test_split(new_df, label, test_size=0.33, random_state=42)

LRC = KNeighborsClassifier()
LRC1 = MultinomialNB()
LRC2 = NearestCentroid()
LRC.fit(train, train_y)
LRC1.fit(train, train_y)
LRC2.fit(train, train_y)

predicted = LRC.predict(test)
precision = precision_score(predicted, test_y, average='macro')

predicted1 = LRC1.predict(test)
precision1 = precision_score(predicted1, test_y, average='macro')

predicted2 = LRC2.predict(test)
precision2 = precision_score(predicted2, test_y, average='macro')

print("KNeighborsClassifier")
print(predicted)
print(precision)
print("\n")

print("MultinomialNB")
print(predicted)
print(precision1)
print("\n")

print("NearestCentroid")
print(predicted)
print(precision2)
print("\n")

plt.bar(["KNeighborsClassifier", "MultinomialNB", "NearestCentroid"], [precision, precision1, precision2], color=('b', 'g', 'r'),  align='center')
plt.grid()
plt.show()

average_precision1 = average_precision_score(test_y, predicted)
precision1, recall1, _ = precision_recall_curve(test_y, predicted)
step_kwargs1 = ({'step': 'post'}
               if 'step' in signature(plt.fill_between).parameters
               else {})
plt.step(recall1, precision1, color='b', alpha=0.2,
         where='post')
plt.fill_between(recall1, precision1, alpha=0.2, color='b', **step_kwargs1)

plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title('KNeighborsClassifier 2-class Precision-Recall curve: AP={0:0.2f}'.format(
          average_precision1))
my_array = np.arange(0, test.shape[0], 1)
plt.show()