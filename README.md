# Binary Classification

On a testé 3 algorithmes : MultinomialNB, NearestCentroid et KNeighborsClassifier. 
Après avoir comparé les précisions des différents algorithmes, KNeighborsClassifier s'avère être l'algorithme le plus performant avec une précision de plus de 98%.
Avec un taux de précision comme celui-ci, on n'a pas jugé nécessaire de tester d'autres algorithmes.

## Installation

Utilisez le package manager pip pour installer les dépendances.

```bash
pip install pandas
pip install scikit-learn
```

## Usage

```bash
python secu.py
```